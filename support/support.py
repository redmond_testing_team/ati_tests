"""
It's wrapper module for selenium operations
"""

import time
import datetime
import os
from selenium import webdriver
from selenium.common.exceptions import WebDriverException
from Initializer import Selenium


class Support:
    _selenium = None

    def __init__(self):
        self._selenium = Selenium.get_instance()

    def comment(self, message):
        print(message)

    def type(self, locator, value):
        self.wait_for_element_is_visible(locator)
        self.comment("I fill field '%s' with value '%s'" % (locator, value))
        self._selenium.find_element_by_xpath(locator).send_keys(value)

    def upload_image(self, locator, value):
        self.comment("I try to upload image '%s' with value '%s'" % (locator, value))
        path = value
        self._selenium.find_element_by_xpath(locator).send_keys(path)

    def clear(self, locator):
        self.comment("I clear field '%s'" % locator)
        self._selenium.find_element_by_xpath(locator).clear()

    def click(self, locator):
        self.wait_for_element_is_visible(locator)
        self.comment("I click on element '%s'" % locator)
        self._selenium.find_element_by_xpath(locator).click()

    def move_mouse_over(self, locator):
        self.wait_for_element_is_visible(locator)
        self.comment("I move mouse over the element '%s'" % locator)
        element = self._selenium.find_element_by_xpath(locator)
        target_element = webdriver.ActionChains(self._selenium).move_to_element(element).perform()

        return target_element

    def wait_for_element_is_not_visible(self, locator):
        self.comment("I wait for element '%s' to be not visible on the page" % locator)
        for i in range(10):
            if not self._selenium.find_element_by_xpath(locator).is_displayed():

                return
            time.sleep(1)

        raise WebDriverException("Element: %s is visible on the page" % locator)

    def wait_for_element_is_visible(self, locator):
        self.comment("I wait for element '%s' to be visible on the page" % locator)
        for i in range(10):
            element = self._selenium.find_element_by_xpath(locator)
            self.comment("I found the element")
            if element.is_displayed():
                
                return
            time.sleep(1)

        raise WebDriverException("Element: %s is not visible on the page" % locator)

    def verify_element_value(self, value, locator):
        self.wait_for_element_is_visible(locator)
        self.comment("I check that element's '%s' value is '%s'" % (locator, value))
        real_value = self._selenium.find_element_by_xpath(locator).get_attribute('value')
        if value != real_value:
            raise WebDriverException("Expected value of the element '%s' is '%s'. "
                                     "Real value is '%s'" % (locator, value, real_value))
        else:
            print('Expected and real values are the same')

    def verify_element_text(self, text, locator):
        self.wait_for_element_is_visible(locator)
        self.comment("I check that element's '%s' text is '%s'" % (locator, text))
        real_text = self._selenium.find_element_by_xpath(locator).text
        if text != real_text:
            raise WebDriverException("Expected text of the element '%s' is '%s'. "
                                     "Real text is '%s'" % (locator, text, real_text))
        else:
            print('Expected and real texts are the same')

    def check_partial_text_value(self, value, locator):
        self.wait_for_element_is_visible(locator)
        string = self._selenium.find_element_by_xpath(locator).text
        if string.find(value) == -1:
            raise WebDriverException('%s does not contains %s' % (string, value))

    def see_in_title(self, title):
        self.comment("I check page title is '%s'" % title)
        real_title = self._selenium.title
        if title != real_title:
            raise WebDriverException("Page title is differ than '%s'. "
                                     "Real title is '%s'" % (title, real_title))

    def see_element(self, locator):
        self.comment("I check that element '%s' is visible on page" % locator)
        if not self._selenium.find_element_by_xpath(locator).is_displayed():
            raise WebDriverException('Element: %s is not visible on page' % locator)

    def dont_see_element(self, locator):
        self.comment("I check that element '%s' is not visible on page" % locator)
        if self._selenium.find_element_by_xpath(locator).is_displayed():
            raise WebDriverException('Element: %s is visible on page' % locator)

    def grab_attribute_value(self, locator, attribute):
        self.comment("I grab '%s' from '%s'" % (attribute, locator))
        attribute_value = self._selenium.find_element_by_xpath(locator).get_attribute(attribute)

        return attribute_value

    def get_alert_text(self):
        alert = self._selenium.switch_to_alert()
        self.comment('I try to get alert text')
        alert_text = alert.text

        return alert_text

    def accept_alert(self):
        alert = self._selenium.switch_to_alert()
        self.comment('I accept the alert')
        alert.accept()

    def dismiss_alert(self):
        alert = self._selenium.switch_to_alert()
        self.comment('I close the alert')
        alert.dismiss()

    def now_date(self, argument):
        date = datetime.date.today()
        if argument != 'dd.mm.yy':
            return date.strftime("%d.%m.%y")
        if argument != 'dd.mm.yyyy':
            return date.strftime("%d.%m.%Y")

    def scroll_page_to_header(self):
        self.comment("I scroll the page to header")
        self._selenium.execute_script("window.scrollBy(0, -document.body.scrollHeight)", "")

    def scroll_page_to_middle(self, pixels):
        self.comment("I scroll the page to middle")
        self._selenium.execute_script("window.scrollBy(0, %s)" % pixels, "")

    def scroll_page_to_bottom(self):
        self.comment("I scroll the page to bottom")
        self._selenium.execute_script("window.scrollBy(0, document.body.scrollHeight)", "")
