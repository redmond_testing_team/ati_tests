import unittest
from selenium import webdriver
from support import autoconfig


class Selenium(object):
    _selenium = None

    def __new__(cls):
        if not hasattr(cls, 'instance'):
            cls.instance  = super(Selenium, cls).__new__(cls)
            chromedriver  = "/usr/local/share/chromedriver"
            cls._selenium = webdriver.Chrome(chromedriver)
        return cls.instance

    @classmethod
    def get_instance(cls):
        if cls._selenium is None:
            chromedriver  = "/usr/local/share/chromedriver"
            cls._selenium = webdriver.Chrome(chromedriver)
        return cls._selenium


class Initializer(unittest.TestCase):
    _selenium = None

    def setUp(self):
        self._selenium = Selenium.get_instance()
        self._selenium.maximize_window()

        self._selenium.get(autoconfig.mainUrl)
        # Set needed wait time count (analog of wait for page to load)
        self._selenium.implicitly_wait(10)

    def tearDown(self):
        self._selenium.quit()
