import Initializer
from pages import RegistrationPage


class WebmasterCampaignsFilter(Initializer.Initializer):
    Initializer.Initializer.setUp(Initializer)
    registration_page = RegistrationPage.RegistrationPage()
    registration_page.check_city_autocomplete_popup_is_not_displayed()
    registration_page.open_city_autocomplete_popup()
    registration_page.select_city_from_popup()
    registration_page.check_city_is_selected(registration_page.spb_name)
    registration_page.check_city_codes_are_displayed(registration_page.spb_city_code)
