import Initializer
from pages import RegistrationPage


class WebmasterCampaignsFilter(Initializer.Initializer):
    Initializer.Initializer.setUp(Initializer)
    registration_page = RegistrationPage.RegistrationPage()
    footer_server_name = registration_page.grab_server_name_from_footer()
    registration_page.open_server_alert()
    alert_server_name = registration_page.grab_server_name_from_alert()
    registration_page.close_server_alert()
    registration_page.verify_server_numbers(footer_server_name, alert_server_name)
