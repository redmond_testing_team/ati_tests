import Initializer
from support import testData
from pages import RegistrationPage


class WebmasterCampaignsFilter(Initializer.Initializer):
    Initializer.Initializer.setUp(Initializer)
    registration_page = RegistrationPage.RegistrationPage()
    registration_page.check_download_btn_is_displayed()
    registration_page.check_logo_is_not_displayed()
    registration_page.check_delete_btn_is_not_displayed()
    registration_page.upload_logo(testData.path_to_logo_file)
    registration_page.check_logo_is_displayed()
    registration_page.check_delete_btn_is_displayed()
