from support import support
import re


class RegistrationPage:
    # Text on the page
    delete_btn_name = 'Удалить'
    server_name     = 'сервер'
    spb_name        = 'Санкт-Петербург, Санкт-Петербург (регион), Россия'
    spb_city_code   = '812'

    # Elements on the page
    register_table          = "//table[@class='register_table']"
    city_name_field         = register_table + \
                              "//input[@id='ctl00_ctl00_main_PlaceHolderMain_atxtGeo_TextBox']"
    city_autocomplete_popup = "//ul[@id='atxtGeo_AutoCompleteBehavior_completionListElem']"
    city_name_in_popup      = city_autocomplete_popup + "/li[@class='AutoCompleteItem'][2]"
    logo_upload_btn         = register_table + "//object[@id='SWFUpload_0']"
    logo_upload_field       = register_table + \
                              "//input[@id='ctl00_ctl00_main_PlaceHolderMain_cpnLogo_fuLogo_" \
                              "ASPXUploader_ASPxUploadControl1_Input_0']"
    logo_area               = register_table + "//div[@id='ctl00_ctl00_main_PlaceHolderMain_cpnLogo']"
    logo_preview_pic        = "//img[@id='imgPreview']"
    logo_delete_btn         = "//a[@id='btnDeleteLogo']"
    logo_progress_wrapper   = "//div[@id='divFileProgress']"
    logo_progress_green     = logo_progress_wrapper + "/div[contains(@class, 'progressGreen')]"
    logo_progress_blue      = logo_progress_wrapper + "/div[contains(@class, 'progressBlue')]"

    city_code_field_tel     = "//input[@id='ctl00_ctl00_main_PlaceHolderMain_FPhoneMainContact_TB2']"
    city_code_field_mob     = "//input[@id='ctl00_ctl00_main_PlaceHolderMain_FPhoneMainContact_TB3']"
    city_code_field_fax     = "//input[@id='ctl00_ctl00_main_PlaceHolderMain_FFax_TB2']"

    site_footer             = "//div[@class='site-footer responsive']"
    server_info             = site_footer + "//div[@class='serverInfo']//a"

    def __init__(self):
        self.support = support.Support()

    def check_download_btn_is_displayed(self):
        self.support.wait_for_element_is_visible(self.logo_upload_btn)

    def upload_logo(self, path):
        self.support.upload_image(self.logo_upload_field, path)

    def check_logo_is_not_displayed(self):
        self.support.wait_for_element_is_not_visible(self.logo_preview_pic)

    def check_logo_is_displayed(self):
        self.support.wait_for_element_is_visible(self.logo_preview_pic)

    def check_delete_btn_is_not_displayed(self):
        self.support.wait_for_element_is_not_visible(
            self.logo_delete_btn + "[contains(text(), '%s')]" % self.delete_btn_name)

    def check_delete_btn_is_displayed(self):
        self.support.wait_for_element_is_visible(
            self.logo_delete_btn + "[contains(text(), '%s')]" % self.delete_btn_name)

    def check_city_name_field_is_displayed(self):
        self.support.wait_for_element_is_visible(self.city_name_field)

    def open_city_autocomplete_popup(self):
        self.support.click(self.city_name_field)
        self.check_city_autocomplete_popup_is_displayed()

    def check_city_autocomplete_popup_is_not_displayed(self):
        self.support.wait_for_element_is_not_visible(self.city_autocomplete_popup)

    def check_city_autocomplete_popup_is_displayed(self):
        self.support.wait_for_element_is_visible(self.city_autocomplete_popup)

    def select_city_from_popup(self):
        self.support.click(self.city_name_in_popup)
        self.check_city_autocomplete_popup_is_not_displayed()

    def check_city_is_selected(self, city):
        self.support.verify_element_value(city, self.city_name_field)

    def check_city_codes_are_displayed(self, code):
        locators = [self.city_code_field_tel, self.city_code_field_mob, self.city_code_field_fax]
        self.support.scroll_page_to_bottom()
        for locator in locators:
            self.support.verify_element_value(code, locator)

    def grab_server_name_from_footer(self):
        server_name = self.support.grab_attribute_value(
            self.server_info + "[contains(text(), '%s')]" % self.server_name, 'text')
        server_number = re.search(r'\d+', server_name).group(0)

        return server_number

    def open_server_alert(self):
        self.support.scroll_page_to_bottom()
        self.support.click(self.server_info + "[contains(text(), '%s')]" % self.server_name)

    def close_server_alert(self):
        self.support.dismiss_alert()

    def grab_server_name_from_alert(self):
        alert_text = self.support.get_alert_text()
        server_number_in_alert = re.search(r'К?к?од.*:.*(\S\d+)', alert_text).group(1)

        return server_number_in_alert

    def verify_server_numbers(self, footer_number, alert_number):
        assert footer_number == alert_number, \
            "Server number in footer and server number in alert are not same"
        self.support.comment('Server number from footer and server number from alert are the same')
